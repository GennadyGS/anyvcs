@echo off
if not exist %1 goto :EOF
for /F %%v in (%~dp0\Registry.txt) do (
	call %~dp0\%%v\Detect.cmd %1
	if not errorlevel 1 call %~dp0\%%v\Run.cmd %*
)
