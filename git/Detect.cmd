if not exist %1 exit 1
if exist %1\ (pushd %1) else (pushd %~dp1)
git rev-parse --is-inside-work-tree
popd
